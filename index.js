/**
 * Задание 5 - Трансформеры и ООП
 *
 * 1. Создать класс Transformer со свойствами name и health (по умолчанию
 *    имеет значение 100) и методом attack()
 * 2. Создать класс Autobot, который наследуется от класса Transformer.
 *    - Имеет свойсто weapon, т.к. автоботы сражаются с использованием оружия.
 *    - Конструктор класса принимает 2 параметра: имя и оружее (Экземпляр класса
 *      Weapon).
 *    - Метод attack возвращает результат использования оружия weapon.fight()
 * 3. Создать класс Deceptikon, который наследуется от класса Transformer.
 *    - Десептиконы не пользуются оружием, поэтому у них нет свойства weapon. Зато
 *      они могут иметь разное количество здоровья.
 *    - Конструктор класса принимает 2 параметра: имя и количество здоровья health
 *    - Метод attack возвращает характеристики стандартного вооружения: { damage: 5, speed: 1000 }
 * 4. Создать класс оружия Weapon, на вход принимает 2 параметра: damage - урон
 *    и speed - скорость атаки. Имеет 1 метод fight, который возвразает характеристики
 *    оружия в виде { damage: 5, speed: 1000 }
 * 5. Создать 1 автобота с именем OptimusPrime с оружием, имеющим характеристики { damage: 100, speed: 1000 }
 * 6. Создать 1 десептикона с именем Megatron и показателем здоровья 10000
 * 7. Посмотреть что происходит при вызове метода atack() у траснформеров разного типа,
 *    посмотреть сигнатуры классов
 *
 * 8. ДЗ-Вопрос: Написаиь симуляцию. Сколько нужно автоботов чтобы победить Мегатрона если параметр speed в оружии это
 *    количество милсекунд до следующего удара?
 */
class Transformer {
    constructor(name, health = 100) {
        this.name = name;
        this.health = health;
    }

    attack() {
    }

    hit(attack) {
        this.health -= attack.damage;
    }
}

class Autobot extends Transformer {
    constructor(name, weapon) {
        super(name);
        this.weapon = weapon;
    }

    attack() {
        return this.weapon.fight();
    }
}

class Deceptikon extends Transformer {

    attack() {
        return {damage: 5, speed: 1000};
    }
}

class Weapon {
    constructor(damage, speed) {
        this.damage = damage;
        this.speed = speed;
    }

    fight() {
        return {damage: this.damage, speed: this.speed};
    }
}

const autoBot1 = new Autobot('OptimusPrime', new Weapon(100, 1000));
const deceptikon1 = new Deceptikon('Megatron', 10000);


console.log(autoBot1.attack())
console.log(deceptikon1.attack())

class Arena {
    constructor(team1, team2) {
        this.team1 = team1;
        this.team2 = team2;
        this.renderer = new Renderer();
    }

    team1Health() {
        return this.team1.reduce((acc, cur) => acc + Math.max(0, cur.health), 0);
    }

    team2Health() {
        return this.team2.reduce((acc, cur) => acc + Math.max(0, cur.health), 0);
    }

    start() {
        let team1curr = 0;
        let team2curr = 0;
        const arena = this;
        let printResult = true;

        function fight(team1Player, team2Player) {
            setTimeout(() => {
                if (arena.team1Health() <= 0 || arena.team2Health() <= 0) {
                    if (printResult) {
                        arena.logResults()
                        printResult = false;
                    }
                    return;
                }

                if (team1Player.health > 0 && team2Player.health > 0) {
                    team1Player.hit(team2Player.attack());
                }
                if (team1Player.health > 0 && team2Player.health > 0) {
                    fight(team2Player, team1Player);
                }
                team1curr++;
                if (team1curr < arena.team1.length) {
                    fight(arena.team1[team1curr], arena.team2[team2curr]);
                } else {
                    team1curr = 0;
                    team2curr++;
                    if (team2curr < arena.team2.length) {
                        fight(arena.team1[team1curr], arena.team2[team2curr]);
                    } else {
                        team2curr = 0;
                        fight(arena.team1[team1curr], arena.team2[team2curr]);
                    }
                }
                arena.renderer.render(arena.team1.map(a => a.health), arena.team2.map(a => a.health));
            }, team2Player.attack().speed)
        }

        let team1Player = this.team1[team1curr];
        let team2Player = this.team2[team2curr];
        this.logStart();
        fight(team1Player, team2Player);
    }

    logResults() {
        console.log('')
        console.log(' ======== Team 1 results ==========')
        console.log(JSON.stringify(this.team1))
        console.log(' ======== Team 2 results ==========')
        console.log(JSON.stringify(this.team2))
        console.log('')
    }

    logStart() {
        console.log('')
        console.log(' ======== Team 1 ==========')
        console.log(JSON.stringify(this.team1))
        console.log(' ======== Team 2 ==========')
        console.log(JSON.stringify(this.team2))
        console.log('')
    }


    startFast() {
        while (this.team1Health() > 0 && this.team2Health() > 0) {

            for (let i = 0; i < this.team2.length; i++) {
                const team2Player = this.team2[i];
                for (let j = 0; j < this.team1.length; j++) {
                    const team1Player = this.team1[j];
                    if (team1Player.health > 0 && team2Player.health > 0) {
                        console.log(`Fight start. Team1 player hits.
                    Team1 player: ${JSON.stringify(team1Player)}
                    Team2 player: ${JSON.stringify(team2Player)}`
                        )
                        team1Player.hit(team2Player.attack());
                        console.log(`Fight end. 
                    Team1 player: ${JSON.stringify(team1Player)}
                    Team2 player: ${JSON.stringify(team2Player)}`
                        )
                    }

                    if (team1Player.health > 0 && team2Player.health > 0) {
                        console.log(`Fight start. Team2 player hits.
                    Team1 player: ${JSON.stringify(team1Player)}
                    Team2 player: ${JSON.stringify(team2Player)}`
                        )
                        team2Player.hit(team1Player.attack());
                        console.log(`Fight end. 
                    Team1 player: ${JSON.stringify(team1Player)}
                    Team2 player: ${JSON.stringify(team2Player)}`
                        )
                    }
                }
            }
        }
        this.logResults();
    }
}

class Renderer {

    render(hps1, hps2) {
        const side1Element = document.querySelector('.arena-side-1');
        const side2Element = document.querySelector('.arena-side-2');
        side1Element.textContent = '';
        side2Element.textContent = '';
        hps1.forEach(hp => {
            side1Element.append(this.generateBot(hp));
        })

        hps2.forEach(hp => {
            side2Element.append(this.generateBot(hp));
        })
    }

    generateBot(hp) {
        const bot = document.createElement('div');
        bot.classList.add('bot');
        const hpText = document.createElement('span');
        if (hp <= 0) {
            bot.style = 'display: none';
        } else if (hp > 0 && hp <= 50) {
            hpText.classList.add('bot__hp-low');
        }
        hpText.textContent = `${hp} hp`;
        bot.append(hpText)
        return bot;
    }
}

function startFight() {
    let autobotElements = document.querySelectorAll('.arena-side-1 .bot');
    let deceptionElements = document.querySelectorAll('.arena-side-2 .bot');

    //create Autobots with different number for Weapon damage
    const autobots = Array.from(autobotElements).map((t, i) => {
        return new Autobot(`Bot ${i}`, new Weapon((i + 1) * 100 - (i + 1) * 10, 1010));
    })
    const deceptikons = Array.from(deceptionElements).map((t, i) => {
        return new Deceptikon(`Megatron ${i}`, 10000);
    })

    const arena3 = new Arena(autobots, deceptikons);

    arena3.start();
}